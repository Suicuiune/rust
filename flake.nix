{
  outputs = { self }: {
    templates = rec {
      default = rust;
      rust = {
        path = ./rust;
	description = "my rust flake";
      };
      bevy = {
        path = ./bevy;
	description = "my bevy flake";
      };
    };
  };
}
