{
  inputs = {
    naersk.url = "github:nix-community/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
	rust-packages = with pkgs; [ cargo rustc rustfmt pre-commit rustPackages.clippy rust-analyzer ];
	bevy-packages = with pkgs; [ udev alsa-lib vulkan-loader libxkbcommon wayland ];
      in
      {
        defaultPackage = naersk-lib.buildPackage ./.;
        devShell = with pkgs; mkShell rec {
	  buildInputs = rust-packages ++ bevy-packages;
	  nativeBuildInputs = [ pkgs.pkg-config ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
	  LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
        };
      });
}
